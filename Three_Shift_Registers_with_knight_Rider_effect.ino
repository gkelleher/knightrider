int latchPin = 8;
int clockPin = 12;
int dataPin = 11;

int values[] = {1, 2, 4, 8, 16, 32, 64, 128};

void setup() {
  pinMode(latchPin, OUTPUT);
  pinMode(clockPin, OUTPUT);
  pinMode(dataPin, OUTPUT);
}

void loop() {

  int index;
  int value1, value2, value3; 
  
  for (int i = 0; i < 20; i++) {
    // take the latchPin low so  
    // the LEDs don't change while you're sending in bits:
    digitalWrite(latchPin, LOW);
    // shift out the bits:
    index = i%8;
    if(i < 8)
    {
      value1 = values[index];
      value2 = 0;
      value3 = 0;
    } else if (i < 16) {
      value1 = 0;
      value2 = values[index];
      value3 = 0;
    } 
    else {
      value1 = 0;
      value2 = 0;
      value3 = values[index];
    }

    shiftOut(dataPin, clockPin, MSBFIRST, value3);  
    shiftOut(dataPin, clockPin, MSBFIRST, value2); 
    shiftOut(dataPin, clockPin, MSBFIRST, value1); 

    //take the latch pin high so the LEDs will light up:
    digitalWrite(latchPin, HIGH);
    // pause before next value:
    delay(15);
    
  }

}
